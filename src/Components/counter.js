import React, { Component } from 'react'

class counter extends Component {
    render() {
        return (
            <div>
                <button onClick = {this.props.increment.bind(this, this.props.counter.id)}>+</button>
                {this.props.counter.value}
                <button onClick = {this.props.decrement.bind(this, this.props.counter.id)}>-</button>
                <button onClick = {this.props.reset.bind(this, this.props.counter.id)}>Reset</button>
            </div>
        )
    }
}

export default counter
