import React, { Component } from 'react'
import Counter from './counter'

class counters extends Component {
    state = {
        counters: [{
            value: 0,
            id: 0
        }]
    }
    increment = (id) => {
        this.setState({counters: this.state.counters.map(counter => {
            if(counter.id === id){
              counter.value +=1
            }
            return counter})})
    }
    decrement = (id) => {
        this.setState({counters: this.state.counters.map(counter => {
            if(counter.id === id){
              counter.value -=1
            }
            return counter})})}

    addCounter = () => {
        let count = {
            value: 0,
            id: this.state.counters.length + 1
        }
        console.log(count)
        this.setState({counters: this.state.counters.concat(count)})
    }

    reset = (id) => {
        this.setState({counters: this.state.counters.map(counter => {
            if(counter.id === id){
              counter.value = 0
            }
            return counter})})
    }
    render() {
        return (
            <div>
                {this.state.counters.map((counter => (
                    <Counter counter = {counter} increment = {this.increment} decrement = {this.decrement} addCounter = {this.addCounter} reset = {this.reset} key = {counter.id} />
                    )))}
                <button onClick = {this.addCounter}>Add</button>
            </div>
        )
    }
}

export default counters
